# Windows 10 Lockpapers

Update: as of 5/25/2021 - looks like most, if not all, of Windows10 "Lockpapers" have been downloaded. Need to check to see if the "random" GUI-like string in the image names are actually repeat of images.

Lockscreen Portrait and Landscape images downloaded from Windows 10.

Seems like these files are often refreshed every Thursday, and sometimes Tuesday, each week. Some are new images, others are older images. 

After about a year of running this program every few days I still get some new images.

The Landscape folder contains 1920x1080p images, and Portrait folder has 1080x1920p images (suitable for phone backgrounds/lockscreens).

[View this Program's code](https://gitlab.com/wilcoforr/windows10lockpapers/blob/master/Program.cs)

